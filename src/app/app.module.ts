import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatSliderModule,
  MatInputModule,
  MatIconModule,
  MatSelectModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatRippleModule,
  MatSnackBarModule,
  MatButtonModule,
  MatTabsModule
} from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { ReservationService } from './reservation.service';
import { HttpClientModule } from '@angular/common/http';
import { TablesComponent } from './tables/tables/tables.component';

import { ChatDialogModule } from './chat/chat-dialog/chat-dialog.module';
import { ChatDialogService } from './chat/chat-dialog.service';

@NgModule({
  declarations: [AppComponent, TablesComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatSliderModule,
    ReactiveFormsModule,
    MatInputModule,
    MatNativeDateModule,
    MatButtonModule,
    MatTabsModule,
    MatRippleModule,
    MatSnackBarModule,
    MatIconModule,
    MatSelectModule,
    MatDatepickerModule,
    FormsModule,
    ChatDialogModule
  ],
  providers: [ReservationService, ChatDialogService],
  bootstrap: [AppComponent]
})
export class AppModule {}
